package com.tpp.mixcloud.retrofit

import com.tpp.mixcloud.CloudCast
import com.tpp.mixcloud.CloudCastsResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query


interface MixCloudService {
    @GET("{user}/cloudcasts/")
    fun listShows(
        @Path("user") user: String,
        @Query("limit") limit: Int): Call<CloudCastsResponse>
}