package com.tpp.mixcloud

import com.google.gson.annotations.SerializedName

class CloudCastsResponse {
    @SerializedName("data")
    lateinit var cloudCasts: List<CloudCast>
}