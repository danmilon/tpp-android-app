package com.tpp.mixcloud

import com.google.gson.annotations.SerializedName
import java.util.*

data class CloudCast (
    @SerializedName("audio_length")
    val audioLength: Int,

    @SerializedName("comment_count")
    val commentCount: Int,

    @SerializedName("created_time")
    val createdTime: Date,

    @SerializedName("listener_count")
    val listenerCount: Int,

    val name: String,

    @SerializedName("play_count")
    val playCount: Int,

    val slug: String,

    @SerializedName("updated_time")
    val updatedTime: String
)