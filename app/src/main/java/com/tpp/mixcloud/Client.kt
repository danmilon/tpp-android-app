package com.tpp.mixcloud

import com.google.gson.GsonBuilder
import com.tpp.mixcloud.retrofit.MixCloudService
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

private const val ISO_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZ"

class Client {
    private val gson = GsonBuilder().run {
        setDateFormat(ISO_FORMAT)
        create()
    }

    private val retrofit = Retrofit.Builder().run {
        baseUrl("https://api.mixcloud.com/")
        addConverterFactory(GsonConverterFactory.create())
        build()
    }

    private val service = retrofit.create(MixCloudService::class.java)

    fun getShows(user: String, limit: Int = 10): CloudCastsResponse? {
        val repos = service.listShows("TPPRadio", limit)
        return repos.execute().body()
    }
}