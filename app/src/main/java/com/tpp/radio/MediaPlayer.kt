package com.tpp.radio

import android.media.AudioAttributes


class MediaPlayer : android.media.MediaPlayer() {
    private val audioAttributes = AudioAttributes.Builder()
        .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
        .setUsage(AudioAttributes.USAGE_MEDIA)
        .build()

    init {
        this.apply {
            setAudioAttributes(audioAttributes)
        }
    }
}