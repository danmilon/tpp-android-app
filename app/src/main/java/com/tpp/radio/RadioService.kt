package com.tpp.radio

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.Intent.ACTION_MEDIA_BUTTON
import android.drm.DrmStore
import android.graphics.BitmapFactory
import android.media.AudioManager
import android.media.MediaPlayer
import android.os.Build
import android.os.Bundle
import android.support.v4.media.MediaBrowserCompat
import android.support.v4.media.MediaMetadataCompat
import android.support.v4.media.session.MediaControllerCompat
import android.support.v4.media.session.MediaSessionCompat
import android.support.v4.media.session.PlaybackStateCompat
import android.util.Log
import android.widget.Toast
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.graphics.createBitmap
import androidx.media.AudioAttributesCompat
import androidx.media.AudioFocusRequestCompat
import androidx.media.AudioManagerCompat
import androidx.media.AudioManagerCompat.AUDIOFOCUS_GAIN
import androidx.media.MediaBrowserServiceCompat
import androidx.media.session.MediaButtonReceiver
import com.tpp.MainActivity
import com.tpp.R
import com.tpp.radio.MediaPlayer as TPPMediaPlayer


const val ACTION_PLAY = "com.tpp.radio.ACTION_PLAY"

private const val TAG = "TPPRadioService"
private const val NOTIFICATION_CHANNEL_ID = "com.tpp.radio.RADIO_NOTIFICATION_CHANNEL"
private const val NOTIFICATION_ID = 145321
private const val MEDIA_ROOT_ID = "ROOT_ID"

private val liveRadioMediaItem = MediaMetadataCompat.Builder().run {
    putString(MediaMetadataCompat.METADATA_KEY_MEDIA_ID, "LIVE_RADIO_ID")
    putString(MediaMetadataCompat.METADATA_KEY_TITLE, "TPP Radio Live")
    putString(MediaMetadataCompat.METADATA_KEY_ARTIST, "The Press Project")
    putString(MediaMetadataCompat.METADATA_KEY_MEDIA_URI, "https://stream.radiojar.com/qgra821mrtwtv.mp3")
    build()
}


class RadioService : MediaBrowserServiceCompat(), AudioManager.OnAudioFocusChangeListener {
    private lateinit var mediaSession: MediaSessionCompat
    private val mediaSessionCallback = object : MediaSessionCompat.Callback() {
        override fun onStop() {
            this@RadioService.stop()
        }

        override fun onPlay() {
            this@RadioService.play()
        }

        override fun onPlayFromMediaId(mediaId: String, extras: Bundle) {
            // Only Live Radio playback is supported ATM.
            assert(mediaId == liveRadioMediaItem.description.mediaId)
            mediaPlayer.setDataSource(liveRadioMediaItem.description.mediaUri?.toString())
            mediaSession.setMetadata(liveRadioMediaItem)
            this@RadioService.play()
        }

        override fun onPause() {
            this@RadioService.pause()
        }
    }

    private val mediaPlayerCallbacks = object :
        MediaPlayer.OnPreparedListener,
        MediaPlayer.OnErrorListener,
        MediaPlayer.OnBufferingUpdateListener,
        MediaPlayer.OnInfoListener {
        override fun onPrepared(mp: MediaPlayer) {
            val focusSuccess = requestAudioFocus()
            if (focusSuccess) {
                onAudioFocusChange(AudioManager.AUDIOFOCUS_GAIN)
            }
        }

        override fun onError(mp: MediaPlayer?, what: Int, extra: Int): Boolean {
            Toast.makeText(
                this@RadioService,
                "MediaPlayer error: ($what, $extra)", Toast.LENGTH_LONG)
                .show()

            Log.e(TAG, "MediaPlayer got error ($what, $extra)")
            return false
        }

        override fun onBufferingUpdate(mp: MediaPlayer?, percent: Int) {
            Log.i(TAG, "Buffering: $percent")
        }

        override fun onInfo(mp: MediaPlayer?, what: Int, extra: Int): Boolean {
            Log.i(TAG, "MP Info: What: $what, Extra: $extra")
            return false
        }
    }

    private val mediaPlayer = TPPMediaPlayer().apply {
        setOnPreparedListener(mediaPlayerCallbacks)
        setOnErrorListener(mediaPlayerCallbacks)
        setOnBufferingUpdateListener(mediaPlayerCallbacks)
        setOnInfoListener(mediaPlayerCallbacks)
    }

    private lateinit var notificationChannel: NotificationChannel

    override fun onCreate() {
        super.onCreate()
        createNotificationChannel()
        createMediaSession()
    }

    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationChannel = NotificationChannel(
                NOTIFICATION_CHANNEL_ID,
                "TPP Radio",
                NotificationManager.IMPORTANCE_LOW).apply {
                    description = getString(R.string.notification_channel_radio_desc)
            }

            val notificationManager: NotificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(notificationChannel)
        }
    }

    private fun createMediaSession() {
        mediaSession = MediaSessionCompat(baseContext, "my-tag").apply {
            // Set an initial PlaybackState with ACTION_PLAY, so media buttons can start the player
            val playbackState = PlaybackStateCompat.Builder()
                .setActions(
                    PlaybackStateCompat.ACTION_PLAY or
                    PlaybackStateCompat.ACTION_PLAY_PAUSE or
                    PlaybackStateCompat.ACTION_STOP
                )
                .build()

            setPlaybackState(playbackState)
            setCallback(mediaSessionCallback)

            // Set the session's token so that client activities can communicate with it.
            setSessionToken(sessionToken)
        }

        mediaSession.controller.registerCallback(object : MediaControllerCompat.Callback() {
            override fun onPlaybackStateChanged(state: PlaybackStateCompat) {
                this@RadioService.updateNotification(state)
            }
        })
    }

    override fun onGetRoot(
        clientPackageName: String,
        clientUid: Int,
        rootHints: Bundle?
    ): MediaBrowserServiceCompat.BrowserRoot {
        // Returns a root ID that clients can use with onLoadChildren() to retrieve
        // the content hierarchy.
        return MediaBrowserServiceCompat.BrowserRoot(MEDIA_ROOT_ID, null)
    }

    override fun onLoadChildren(
        parentMediaId: String,
        result: MediaBrowserServiceCompat.Result<List<MediaBrowserCompat.MediaItem>>
    ) {
        // Only the Live Radio is supported now.
        assert(parentMediaId == MEDIA_ROOT_ID)

        val mediaItems = listOf<MediaBrowserCompat.MediaItem>(
            MediaBrowserCompat.MediaItem(
                liveRadioMediaItem.description,
                MediaBrowserCompat.MediaItem.FLAG_PLAYABLE
            )
        )

        result.sendResult(mediaItems)
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        when(intent.action) {
            ACTION_PLAY -> {
                mediaSession.controller.transportControls.play()
            }
            ACTION_MEDIA_BUTTON -> {
                MediaButtonReceiver.handleIntent(mediaSession, intent)
            }
        }

        return START_STICKY
    }

    private fun requestAudioFocus(): Boolean {
        val focusRequest = AudioFocusRequestCompat.Builder(AUDIOFOCUS_GAIN)
            .setOnAudioFocusChangeListener(this)
            .setAudioAttributes(AudioAttributesCompat.Builder()
                .setContentType(AudioAttributesCompat.CONTENT_TYPE_MUSIC)
                .setUsage(AudioAttributesCompat.USAGE_MEDIA)
                .build()
            )
            .build()

        val audioManager = getSystemService(Context.AUDIO_SERVICE) as AudioManager
        val result = AudioManagerCompat.requestAudioFocus(
            audioManager,
            focusRequest
        )

        return result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED
    }

    private fun setPlaybackState(actions: Long, state: Int) {
        var currPos: Int;
        if (mediaPlayer.isPlaying) {
            currPos = mediaPlayer.currentPosition
        }
        else {
            currPos = 0
        }

         mediaSession.setPlaybackState(PlaybackStateCompat.Builder()
            .setActions(actions)
            .setState(
                state,
                currPos.toLong(),
                1.0f
            )
            .build())
    }

    private fun updateNotification(state: PlaybackStateCompat) {
        if ((state.state == PlaybackStateCompat.STATE_STOPPED) or (state.state == PlaybackStateCompat.STATE_NONE)) {
            with(NotificationManagerCompat.from(this@RadioService)) {
                cancel(NOTIFICATION_ID)
            }

            return
        }

        val mediaMeta = mediaSession.controller.metadata
        val builder = NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)

        builder.apply {
            setSmallIcon(R.drawable.ic_tpp_logo)
            priority = NotificationCompat.PRIORITY_LOW

            // Show the UI when tapped.
            setContentIntent(
                PendingIntent.getActivity(
                    this@RadioService,
                    0,
                    Intent(this@RadioService, MainActivity::class.java).apply {
                        flags = Intent.FLAG_ACTIVITY_NEW_TASK or
                                Intent.FLAG_ACTIVITY_CLEAR_TASK
                    },
                    0
                )
            )

            // Stop the service when the notification is swiped away
            setDeleteIntent(
                MediaButtonReceiver.buildMediaButtonPendingIntent(
                    this@RadioService,
                    PlaybackStateCompat.ACTION_STOP
                )
            )

            addAction(
                R.drawable.ic_close_black_24dp,
                getString(R.string.notification_action_stop_title),
                MediaButtonReceiver.buildMediaButtonPendingIntent(
                    this@RadioService,
                    PlaybackStateCompat.ACTION_STOP
                )
            )

            when (state.state) {
                PlaybackStateCompat.STATE_CONNECTING,
                PlaybackStateCompat.STATE_BUFFERING -> {
                    setContentTitle(getString(R.string.loading))
                    setProgress(0, 0, true)
                }
                PlaybackStateCompat.STATE_PLAYING,
                PlaybackStateCompat.STATE_PAUSED -> {
                    if (mediaMeta != null) {
                        val mediaDesc = mediaMeta.description
                        setContentTitle(mediaDesc.title)
                        setContentText(mediaDesc.subtitle)
                        setSubText(mediaDesc.description)
                        setLargeIcon(mediaDesc.iconBitmap)
                    }
                    else {
                        setContentTitle("Unknown media")
                    }

                    // Remove progress bar.
                    setProgress(0, 0, false)

                    if (state.state == PlaybackStateCompat.STATE_PLAYING) {
                        addAction(
                            R.drawable.ic_pause_black_24dp,
                            getString(R.string.notification_action_pause_title),
                            MediaButtonReceiver.buildMediaButtonPendingIntent(
                                this@RadioService,
                                PlaybackStateCompat.ACTION_PAUSE
                            )
                        )
                    }
                    else if (state.state == PlaybackStateCompat.STATE_PAUSED) {
                        addAction(
                            R.drawable.ic_play_arrow_black_24dp,
                            getString(R.string.play),
                            MediaButtonReceiver.buildMediaButtonPendingIntent(
                                this@RadioService,
                                PlaybackStateCompat.ACTION_PLAY
                            )
                        )
                    }

                    // Make the transport controls visible on the lock screen.
                    setVisibility(NotificationCompat.VISIBILITY_PUBLIC)


                    val style = androidx.media.app.NotificationCompat.MediaStyle()
                        .setMediaSession(mediaSession.sessionToken)
                        // Compat.
                        .setShowCancelButton(true)
                        .setCancelButtonIntent(
                            MediaButtonReceiver.buildMediaButtonPendingIntent(
                                this@RadioService,
                                PlaybackStateCompat.ACTION_STOP
                            )
                        )

                    // Show all actions in compact view.
                    for (idx in 0 until mActions.size) {
                        style.setShowActionsInCompactView(idx)
                    }

                    setStyle(style)
                }
                else -> {
                    Log.w(TAG, "Unknown state ${mediaSession.controller.playbackState.state}")
                }
            }
        }

        val notification = builder.build()
        with(NotificationManagerCompat.from(this)) {
            startForeground(NOTIFICATION_ID, notification)
        }
    }

    private fun play() {
        mediaSession.isActive = true

        val state = mediaSession.controller.playbackState
        when (state.state) {
            PlaybackStateCompat.STATE_PAUSED -> {
                mediaPlayer.start()
                setPlaybackState(
                    PlaybackStateCompat.ACTION_STOP or
                            PlaybackStateCompat.ACTION_PAUSE,
                    PlaybackStateCompat.STATE_PLAYING)
            }
            PlaybackStateCompat.STATE_STOPPED,
            PlaybackStateCompat.STATE_NONE-> {
                mediaPlayer.prepareAsync()
                setPlaybackState(
                    PlaybackStateCompat.ACTION_STOP,
                    PlaybackStateCompat.STATE_CONNECTING
                )
            }
            else -> {
                throw Exception("Cannot play from state ${state}")
            }
        }

        updateNotification(state)
    }

    private fun stop() {
        mediaPlayer.reset()
        mediaSession.isActive = false
        stopSelf()
        setPlaybackState(
            PlaybackStateCompat.ACTION_PLAY,
            PlaybackStateCompat.STATE_STOPPED
        )

        stopForeground(true)
    }

    fun pause() {
        mediaPlayer.pause()
        mediaSession.isActive = false
        setPlaybackState(
            PlaybackStateCompat.ACTION_PLAY or
                    PlaybackStateCompat.ACTION_STOP,
            PlaybackStateCompat.STATE_PAUSED
        )
    }

    override fun onAudioFocusChange(focusChange: Int) {
        // TODO: Also update the playback state.
        when (focusChange) {
            AudioManager.AUDIOFOCUS_GAIN -> {
                if (!mediaPlayer.isPlaying) {
                    mediaPlayer.start()
                    setPlaybackState(
                        PlaybackStateCompat.ACTION_STOP or
                                PlaybackStateCompat.ACTION_PAUSE,
                        PlaybackStateCompat.STATE_PLAYING
                    )
                }

                mediaPlayer.setVolume(1.0f, 1.0f);
            }
            AudioManager.AUDIOFOCUS_LOSS -> {
                if (mediaPlayer.isPlaying) {
                    mediaPlayer.stop()
                }
            }
            AudioManager.AUDIOFOCUS_LOSS_TRANSIENT -> {
                mediaPlayer.pause()
            }
            AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK -> {
                mediaPlayer.setVolume(0.3f, 0.3f)
            }
        }
    }
}
