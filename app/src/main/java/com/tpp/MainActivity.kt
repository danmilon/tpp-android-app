package com.tpp

import android.content.ComponentName
import android.content.Intent
import android.media.AudioManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.support.v4.media.MediaBrowserCompat
import android.support.v4.media.session.MediaControllerCompat
import android.support.v4.media.session.PlaybackStateCompat
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import com.tpp.radio.RadioService
import kotlinx.android.synthetic.main.activity_main.*


private const val CHAT_URL = "https://chat.thepressproject.gr"


class MainActivity : AppCompatActivity() {
    private lateinit var mediaBrowser: MediaBrowserCompat
    private lateinit var liveRadioMedia: MediaBrowserCompat.MediaItem

    private val chatWebViewClient = object : WebViewClient() {
        override fun shouldOverrideUrlLoading(
            view: WebView,
            url: String
        ): Boolean {
            val uri = Uri.parse(url)
            if (uri.host == "chat.thepressproject.gr") {
                view.loadUrl(url)
            }
            else {
                // TODO: Doesn't work
                startActivity(Intent(Intent.ACTION_VIEW, uri))
            }

            return true
        }

        override fun onPageFinished(view: WebView, url: String) {
            chatWebViewProgressBar.isVisible = false
            webview.isVisible = true
        }
    }

    private val mediaControllerCallback = object : MediaControllerCompat.Callback() {
        override fun onPlaybackStateChanged(state: PlaybackStateCompat) {
            val itemPlay = toolbar.menu.findItem(R.id.action_play)
            val itemStop = toolbar.menu.findItem(R.id.action_stop)

            when (state.state) {
                PlaybackStateCompat.STATE_CONNECTING -> {
                    itemPlay.isVisible = false
                    itemStop.isVisible = true
                }
                PlaybackStateCompat.STATE_STOPPED -> {
                    itemPlay.isVisible = true
                    itemStop.isVisible = false
                }
            }
        }
    }

    private val mediaBrowserCallback = object: MediaBrowserCompat.ConnectionCallback() {
        override fun onConnected() {
            // Get the token for the MediaSession
            mediaBrowser.sessionToken.also { token ->
                // Create a MediaControllerCompat
                val mediaController = MediaControllerCompat(
                    this@MainActivity,
                    token
                )

                // Save the controller
                MediaControllerCompat.setMediaController(this@MainActivity, mediaController)
            }

            mediaBrowser.subscribe(mediaBrowser.root, object: MediaBrowserCompat.SubscriptionCallback() {
                override fun onChildrenLoaded(
                    parentId: String,
                    children: MutableList<MediaBrowserCompat.MediaItem>
                ) {
                    // We know it's the only one.
                    liveRadioMedia = children[0]
                }
            })

            // Finish building the UI
            buildTransportControls()
        }

        override fun onConnectionSuspended() {
            // The Service has crashed. Disable transport controls until it automatically reconnects
        }

        override fun onConnectionFailed() {
            // The Service has refused our connection
        }

        fun buildTransportControls() {
            val mediaController = MediaControllerCompat.getMediaController(this@MainActivity)
            mediaController.registerCallback(mediaControllerCallback)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        mediaBrowser = MediaBrowserCompat(
            this,
            ComponentName(this, RadioService::class.java),
            mediaBrowserCallback,
            null // optional Bundle
        )

        // Chat WebView
        webview.apply {
            settings.javaScriptEnabled = true

            // trying to fix login issue. no luck.
            settings.javaScriptCanOpenWindowsAutomatically = true
            settings.setSupportMultipleWindows(true)

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                // Prevent it from querying to Google Safe Browsing.
                settings.safeBrowsingEnabled = false
            }

            loadUrl(CHAT_URL)

            webViewClient = chatWebViewClient
        }
    }

    public override fun onStart() {
        super.onStart()
        mediaBrowser.connect()
    }

    public override fun onResume() {
        super.onResume()
        volumeControlStream = AudioManager.STREAM_MUSIC
    }

    public override fun onStop() {
        super.onStop()
        MediaControllerCompat.getMediaController(this)?.unregisterCallback(mediaControllerCallback)
        mediaBrowser.disconnect()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.toolbar_actions, menu)
        return true
    }

    private fun startRadioService() {
        val intent = Intent(this, RadioService::class.java)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(intent)
        }
        else {
            startService(intent)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.action_play -> {
            // Even though the service is already started because of the MediaBrowser, this ensures
            // that it will keep running in the foreground, otherwise when the MediaBrowser disconnects
            // (on activity destroy) the service will be destroyed as well.
            startRadioService()
            mediaController.transportControls.playFromMediaId(liveRadioMedia.mediaId, Bundle.EMPTY)
            true
        }
        R.id.action_stop -> {
            mediaController.transportControls.stop()
            true
        }
        else -> {
            // If we got here, the user's action was not recognized.
            // Invoke the superclass to handle it.
            super.onOptionsItemSelected(item)
        }
    }
}
